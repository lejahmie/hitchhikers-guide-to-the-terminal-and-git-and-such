########################################################
        _ _       _     _     _ _             _    
  /\  /(_) |_ ___| |__ | |__ (_) | _____ _ __( )__ 
 / /_/ / | __/ __| '_ \| '_ \| | |/ / _ \ '__|/ __|
/ __  /| | || (__| | | | | | | |   <  __/ |   \__ \
\/ /_/ |_|\__\___|_| |_|_| |_|_|_|\_\___|_|   |___/
             _     _                               
  __ _ _   _(_) __| | ___                          
 / _` | | | | |/ _` |/ _ \                         
| (_| | |_| | | (_| |  __/                         
 \__, |\__,_|_|\__,_|\___|                         
 |___/                                             
___  __     ___       ___                         
 |  /  \     |  |__| |__                          
 |  \__/     |  |  | |___                         
                                                  
___  ___  __                                      
 |  |__  |__)  |\/| | |\ |  /\  |                 
 |  |___ |  \  |  | | | \| /~~\ |___              
                                                  
           __      __    ___                      
 /\  |\ | |  \    / _` |  |                       
/~~\ | \| |__/    \__> |  |                       
                                                  
           __      __        __                   
 /\  |\ | |  \    /__` |  | /  ` |__|             
/~~\ | \| |__/    .__/ \__/ \__, |  |   

########################################################

# BASIC TERMINAL #

ls
    List current directory

ls -la
    List current directory, all files with more info

cd <directory name>
    Open a directory and go into it

cd ..
    Go back one step, leave directory

mkdir <some name>
    Create a new directory in current directory

cp <file name> <new file name>
    Copy a file. Note you can also copy using full or relative paths.

rm <file name>
    Deletes a file

rm -rf <directory name>
    Delete a directory and all its content

    NOTE: There is no undo and no trashcan in the Terminal.


# BASIC GIT #

When working with git in the terminal. 
First go to the directory containing the 
git repository you want to work with.
    Example;
        cd /web/site1/
If the directory is a git repository you should 
see a .git directory there.
    Check if it exists;
        ls -la



git pull origin master
    Get latest files from repository

git fetch
    Retrive latest branches and/or tags

git status
    Tells you if you have uncommited changes and/or commits
    waiting to be pushed

git add -A
    All all modified files

git commit
    Make a new commit, you will be promted to write a 
    commit message in the default editor.

git commit -m "Your informative message..."
    Make a new commit, with a message

git push origin master
    Push all commits waiting to be pushed

git stash
    Stash any changes that have not been commited.
    Think of it as a save, but it also removes whatever is saved
    and can later be loaded back

git stash pop
    Load previously stashed changes

git branch
    List all local branches

git branch -a
    List all branches, local and remote

git checkout -b <new branch name>
    Create a new branch and open it

git checkout <branch name>
    Open a existing branch

git checkout <commit id>
    Open up a commit. Go to the state of a existing commit.

git checkout master
    Open current latest local state.

git init
    Create a new repo in the current directory

git clone <git repository adress>
    Clone a repository to the current directory


# Basic git workflow examples #

I want to do some changes and push them to master branch?
    First get the latest:
        git pull origin master

    Now do your work...

    Add all the modified files to be ready for commit:
        git add -A

    Make a commit:
        git commit -m "Your informative message..."

    Push the commit to master branch:
        git push origin master


I allready started working, but forgot to get latest. What now?
    First stash your local changes:
        git stash

    Now get the latest:
        git pull origin master

    Retrive your stashed changes:
        git stash pop

    (In case of conflict)
    Now you might get a merge conflict. If you do, you will
    get a list of files that has conflicts in them.
    Open them and resolve the problems.

    Add all the modified files to be ready for commit:
        git add -A

    Make a commit:
        git commit -m "Your informative message..."

    Push the commit to master branch:
        git push origin master


To be continued....








